-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 27, 2015 at 05:21 PM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mauiapp`
--

-- --------------------------------------------------------

--
-- Table structure for table `mss_branches`
--

DROP TABLE IF EXISTS `mss_branches`;
CREATE TABLE IF NOT EXISTS `mss_branches` (
  `branch_id` int(10) unsigned NOT NULL,
  `store_id` int(11) NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `landmark` text COLLATE utf8_unicode_ci NOT NULL,
  `store_hours` text COLLATE utf8_unicode_ci NOT NULL,
  `telephone_number` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mss_branches`
--

INSERT INTO `mss_branches` (`branch_id`, `store_id`, `address`, `landmark`, `store_hours`, `telephone_number`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, '199 E. Alex Bell Rd. Unit 402, Centerville Ohio 45459', 'next to Doubleday''s and Firestone', '', '(937) 401-0710', '1', '2015-08-06 04:00:00', '2015-08-06 04:00:00', NULL),
(2, 2, '7967 Cincinnati Dayton Rd Unit J, West Chester Ohio 45069', '', '', '(513) 847-6008', '1', '2015-08-06 04:00:00', '2015-08-06 04:00:00', NULL),
(3, 3, '3092 Madison Rd., Cincinnati Ohio 45209', '', '', '(513) 788-2212', '1', '2015-08-06 04:00:00', '2015-08-06 04:00:00', NULL),
(4, 4, '9247 E 141 st. Fishers Indiana 46038', '', '', '(317) 214-7829', '1', '2015-08-06 04:00:00', '2015-08-06 04:00:00', NULL),
(5, 5, '8660 Guion Rd, Indianapolis Indiana 46268', '', '', '(317) 451-4212', '1', '2015-08-06 04:00:00', '2015-08-06 04:00:00', NULL),
(6, 6, '4113 Oechsli Ave Suite F, Louisville Kentucky 40207', '', '', '(502) 938-MAUI or (502) 938-6284', '1', '2015-08-06 04:00:00', '2015-08-06 04:00:00', NULL),
(7, 7, '330 Louisiana Ave. Suite E, Perrysburg Ohio 43551', '', '', '(419) 265-0699', '1', '2015-08-06 04:00:00', '2015-08-06 04:00:00', NULL),
(8, 8, '97 N. Kingshighway Suite 6, Cape Girardeau Missouri 63701', '', '', '573-803-1818', '1', '2015-08-06 04:00:00', '2015-08-06 04:00:00', NULL),
(9, 9, 'Eastern Hills Mall 4545 Transit Rd, Williamsville New York 14221', '', '', '(716) 580-7066', '1', '2015-08-06 04:00:00', '2015-08-06 04:00:00', NULL),
(10, 10, '12751 Marblestone Drive, Suite 240, Woodbridge Virginia 22192', '', '', '(703) 730-6284', '1', '2015-08-06 04:00:00', '2015-08-06 04:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mss_check_in`
--

DROP TABLE IF EXISTS `mss_check_in`;
CREATE TABLE IF NOT EXISTS `mss_check_in` (
  `check_in_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `branch_id` int(10) unsigned NOT NULL,
  `service_id` int(10) unsigned NOT NULL,
  `number_of_guest` smallint(6) NOT NULL,
  `date_check_in` datetime NOT NULL,
  `status` varchar(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `mss_migrations`
--

DROP TABLE IF EXISTS `mss_migrations`;
CREATE TABLE IF NOT EXISTS `mss_migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mss_migrations`
--

INSERT INTO `mss_migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2015_08_09_232147_create_posts_table', 1),
('2015_08_14_192536_create_branches_table', 1),
('2015_08_15_230000_create_check_in_table', 1),
('2015_08_15_232437_create_services_table', 1),
('2015_08_15_232843_create_services_offer_table', 1),
('2015_08_15_233145_create_stores_table', 1),
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2015_08_09_232147_create_posts_table', 1),
('2015_08_14_192536_create_branches_table', 1),
('2015_08_15_230000_create_check_in_table', 1),
('2015_08_15_232437_create_services_table', 1),
('2015_08_15_232843_create_services_offer_table', 1),
('2015_08_15_233145_create_stores_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `mss_password_resets`
--

DROP TABLE IF EXISTS `mss_password_resets`;
CREATE TABLE IF NOT EXISTS `mss_password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `mss_posts`
--

DROP TABLE IF EXISTS `mss_posts`;
CREATE TABLE IF NOT EXISTS `mss_posts` (
  `post_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `content` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mss_posts`
--

INSERT INTO `mss_posts` (`post_id`, `user_id`, `content`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'tex\r\n', 'active', '2015-08-19 12:36:58', '2015-08-19 12:36:58', NULL),
(2, 1, 'data\r\n', 'active', '2015-08-20 09:44:51', '2015-08-20 09:44:51', NULL),
(3, 1, 'help', 'active', '2015-08-20 09:45:57', '2015-08-20 09:45:57', NULL),
(4, 1, 'test', 'active', '2015-08-20 09:46:41', '2015-08-20 09:46:41', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mss_services`
--

DROP TABLE IF EXISTS `mss_services`;
CREATE TABLE IF NOT EXISTS `mss_services` (
  `service_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` double(8,2) NOT NULL,
  `status` varchar(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mss_services`
--

INSERT INTO `mss_services` (`service_id`, `name`, `price`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Laser Teeth Whitening', 0.00, '1', '2015-08-07 04:00:00', '2015-08-07 04:00:00', NULL),
(2, 'Eyelash Extensions', 0.00, '1', '2015-08-07 04:00:00', '2015-08-07 04:00:00', NULL),
(3, 'Medical Weight Loss', 0.00, '1', '2015-08-07 04:00:00', '2015-08-07 04:00:00', NULL),
(4, 'Botox/Fillers', 0.00, '1', '2015-08-07 04:00:00', '2015-08-07 04:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mss_services_offer`
--

DROP TABLE IF EXISTS `mss_services_offer`;
CREATE TABLE IF NOT EXISTS `mss_services_offer` (
  `service_offer_id` int(10) unsigned NOT NULL,
  `service_id` int(10) unsigned NOT NULL,
  `branch_id` int(10) unsigned NOT NULL,
  `status` varchar(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mss_services_offer`
--

INSERT INTO `mss_services_offer` (`service_offer_id`, `service_id`, `branch_id`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(2, 2, 1, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(3, 3, 2, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(4, 4, 2, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(5, 1, 3, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(6, 3, 3, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(7, 2, 4, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(8, 4, 4, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(9, 1, 5, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(10, 1, 6, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(11, 2, 6, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(12, 4, 6, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(13, 1, 7, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(14, 4, 7, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(15, 3, 8, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(16, 4, 8, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(17, 2, 9, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(18, 1, 10, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(19, 3, 10, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(20, 4, 10, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mss_stores`
--

DROP TABLE IF EXISTS `mss_stores`;
CREATE TABLE IF NOT EXISTS `mss_stores` (
  `store_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `status` varchar(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mss_stores`
--

INSERT INTO `mss_stores` (`store_id`, `name`, `user_id`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Maui Whitening Dayton', 3, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(2, 'Maui Whitening West Chester', 4, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(3, 'Maui Whitening Hyde Park / Oakley', 4, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(4, 'Maui Whitening Fishers', 3, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(5, 'Maui Whitening at Indy Health & Fitness', 4, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(6, 'Maui Whitening Louisville', 3, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(7, 'Maui Whitening Perrysburg', 3, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(8, 'Maui Whitening Cape Girardeau', 3, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(9, 'Maui Whitening Williamsville', 4, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(10, 'Maui Whitening Prince William', 4, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mss_users`
--

DROP TABLE IF EXISTS `mss_users`;
CREATE TABLE IF NOT EXISTS `mss_users` (
  `user_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mss_users`
--

INSERT INTO `mss_users` (`user_id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Jemuel Rodriguez', 'jemuel@primeoutsourcing.com', '$2y$10$Q3qjZGAg7TnONy1YzcvX5ObNHUxsrFoBUOYbj2o7evNg8K5DKpuIi', 'CE7PFsgwokUPWLR2XhuZk0xPLwjKfibiBpF3AmUpeeMLKWxVjrxw7PFRPP1W', '2015-08-19 09:42:44', '2015-08-19 09:49:40'),
(2, 'Dummy Accnt', 'dummyaccnt548@gmail.com', '$2y$10$CP18b7WkU6HAVv27/pc.TOidtf1vO71qsXxKjlizncGSV8eEOl9h2', NULL, '2015-08-24 05:09:17', '2015-08-24 05:09:17');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `mss_branches`
--
ALTER TABLE `mss_branches`
  ADD PRIMARY KEY (`branch_id`);

--
-- Indexes for table `mss_check_in`
--
ALTER TABLE `mss_check_in`
  ADD PRIMARY KEY (`check_in_id`);

--
-- Indexes for table `mss_password_resets`
--
ALTER TABLE `mss_password_resets`
  ADD KEY `password_resets_email_index` (`email`), ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `mss_posts`
--
ALTER TABLE `mss_posts`
  ADD PRIMARY KEY (`post_id`);

--
-- Indexes for table `mss_services`
--
ALTER TABLE `mss_services`
  ADD PRIMARY KEY (`service_id`);

--
-- Indexes for table `mss_services_offer`
--
ALTER TABLE `mss_services_offer`
  ADD PRIMARY KEY (`service_offer_id`);

--
-- Indexes for table `mss_stores`
--
ALTER TABLE `mss_stores`
  ADD PRIMARY KEY (`store_id`);

--
-- Indexes for table `mss_users`
--
ALTER TABLE `mss_users`
  ADD PRIMARY KEY (`user_id`), ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `mss_branches`
--
ALTER TABLE `mss_branches`
  MODIFY `branch_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `mss_check_in`
--
ALTER TABLE `mss_check_in`
  MODIFY `check_in_id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mss_posts`
--
ALTER TABLE `mss_posts`
  MODIFY `post_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `mss_services`
--
ALTER TABLE `mss_services`
  MODIFY `service_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `mss_services_offer`
--
ALTER TABLE `mss_services_offer`
  MODIFY `service_offer_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `mss_stores`
--
ALTER TABLE `mss_stores`
  MODIFY `store_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `mss_users`
--
ALTER TABLE `mss_users`
  MODIFY `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
