<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class RestfulController extends Controller
{

    /**
     * Display the _branches.
     *
     * @return Response
     */
    public function showBranches($id = null)
    {
        
        if (empty($id)) {
            $branches = App\Branches::all();
        } else {
            $branches = App\Branches::findOrFail($id);
        }

        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
        header('Access-Control-Allow-Headers: Origin, Content-Type, Accept, Authorization, X-Request-With');
        header('Access-Control-Allow-Credentials: true');

        return $branches;
    }


    /**
     * Display the _services.
     *
     * @return Response
     */
    public function showServices($id = null)
    {   
        if (empty($id)) {
            $services = App\Services::all();
        } else {
            $services = App\Services::findOrFail($id);
        }

        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
        header('Access-Control-Allow-Headers: Origin, Content-Type, Accept, Authorization, X-Request-With');
        header('Access-Control-Allow-Credentials: true');

        return $services;
    }


    /**
     * Display the _stores
     *
     * @return Response
     */
    public function showStores($id = null)
    {
        
        if (empty($id)) {
            $stores = App\Stores::all();    
        } else {
            $stores = App\Stores::findOrFail($id);
        }

        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
        header('Access-Control-Allow-Headers: Origin, Content-Type, Accept, Authorization, X-Request-With');
        header('Access-Control-Allow-Credentials: true');

        return $stores;
    }


    /**
     * Display the _branches which associated with _services
     *
     * @var $id unique key of _services
     * @todo It should be eloquent not \DB
     * @return Response
     */
    public function showServicesWithBranches($id = null)
    {
        $servicesWithBranches = \DB::table('services_offer')
        ->join('branches', 'services_offer.branch_id', '=', 'branches.branch_id')
        ->select('branches.*')
        ->where('services_offer.service_id', $id)
        ->get();
        
        return $servicesWithBranches;
    }
}