<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Services;
use App\Http\Requests\CreateServiceRequest;
use App\Http\Controllers\Controller;

class ServiceController extends Controller
{
    
    /**
     * Security checkpoint.
     *
     * @return Response
     */
    public function __construct()
    {

        $this->middleware('auth');
    }



    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
        return view('service.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(CreateServiceRequest $request)
    {
        //
        $services = new Services;
        $services->name = $request->name;
        $services->price = $request->price;
        $services->save();

        \Session::flash('flash_message', 'Service has been added.');

        return redirect('service/create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
