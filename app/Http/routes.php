<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
Route::get('/', function () {
    return view('welcome');
});
*/
Route::get('/', 'ContentController@index');

// Dashboard
Route::get('dashboard', 'DashboardController@index');

// Authentication routes
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

// Post
Route::get('post/create', 'PostController@create');
Route::post('post/store', 'PostController@store');

// Branch
Route::get('branch/create', 'BranchController@create');
Route::post('branch/store', 'BranchController@store');

// Service
Route::get('service/create', 'ServiceController@create');
Route::post('service/store', 'ServiceController@store');

// Restful
Route::get('rest/branches', 'RestfulController@showBranches');
Route::get('rest/branches/{id}', 'RestfulController@showBranches');

Route::get('rest/services', 'RestfulController@showServices');
Route::get('rest/services/{id}', 'RestfulController@showServices');

Route::get('rest/stores', 'RestfulController@showStores');
Route::get('rest/stores/{id}', 'RestfulController@showStores');

Route::get('rest/services-offer', 'RestfulController@showServicesOffer');
Route::get('rest/services-offer/{id}', 'RestfulController@showServicesOffer');

Route::get('rest/services-with-branches/{id}', 'RestfulController@showServicesWithBranches');