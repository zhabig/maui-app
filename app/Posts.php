<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Posts extends Model
{
    
	
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'posts';
    

    /**
     * This a primary key
     *
     * @var string
     */
    protected $primaryKey = 'post_id';

   
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'content'];
}