<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Mosaddek">
    <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
    <link rel="shortcut icon" href="{{ url() }}/themes/flatlab/img/favicon.png">
    <title>FlatLab - Flat & Responsive Bootstrap Admin Template</title>
    <!-- Bootstrap core CSS -->
    <link href="{{ url() }}/themes/flatlab/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{ url() }}/themes/flatlab/css/bootstrap-reset.css" rel="stylesheet">
    <!--external css-->
    <link href="{{ url() }}/themes/flatlab/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <!-- Custom styles for this template -->
    <link href="{{ url() }}/themes/flatlab/css/style.css" rel="stylesheet">
    <link href="{{ url() }}/themes/flatlab/css/style-responsive.css" rel="stylesheet" />
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
    <script src="{{ url() }}/themes/flatlab/js/html5shiv.js"></script>
    <script src="{{ url() }}/themes/flatlab/js/respond.min.js"></script>
    <![endif]-->
</head>
<body class="login-body">
    <div class="container">
        <form class="form-signin" action="{{ url('/auth/register') }}" method="post">
            
            {!! csrf_field() !!}

            <h2 class="form-signin-heading">registration now</h2>
            <div class="login-wrap">
                <p>Enter your personal details below</p>

                @if (count($errors) > 0)
                <div class="alert alert-block alert-danger fade in">
                    <button type="button" class="close close-sm" data-dismiss="alert">
                      <i class="fa fa-times"></i>
                    </button>
                    <strong>Whoops!</strong> There were some problems with your input.
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif

                <input type="text" class="form-control" placeholder="Full Name" name="name" value="{{ old('name') }}" autofocus>
                
                <input type="text" class="form-control" placeholder="Email" name="email" value="{{ old('email') }}" autofocus>
                
                <input type="password" class="form-control" placeholder="Password" name="password" value="">
                <input type="password" class="form-control" placeholder="Re-type Password" name="password_confirmation" value="">
                <label class="checkbox">
                    <input type="checkbox" value="agree this condition"> I agree to the Terms of Service and Privacy Policy
                </label>
                
                <button class="btn btn-lg btn-login btn-block" type="submit">Submit</button>
                <div class="registration">
                    Already Registered.
                    <a class="" href="{{ url() }}/auth/login">
                        Login
                    </a>
                </div>
            </div>
        </form>
    </div>
</body>
</html>